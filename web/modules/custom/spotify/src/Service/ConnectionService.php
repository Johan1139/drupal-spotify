<?php

namespace Drupal\spotify\Service;

use GuzzleHttp\Exception\RequestException;

/**
 * Class Connection Service.
 */
class ConnectionService {

  /**
   * Request function.
   */
  public static function request($url, $method, $params = []) {

    try {
      /* Initialized curl*/
      $ch = curl_init();

      /* Set header for curl request */
      $headers = [
        "Accept: application/json",
        "Content-Type: application/json",
        "Authorization: Bearer BQDWFXThEQ5rCCFFwvx2PtGGwADgOJDVkJ-er2uNc1pmvJCg-qg_51qtG_EFTjcs3okj7Gt_rst7Pt_z9Yqhws5xgpp_NhDf2dD1MoMa4aiRxQgaXKmc7loTd5VyPd5SRyQ2PeMyIc3sK-Zniz4kbn7H_rgTx-G6tWk8mKTpt46KcRp_39ezbhKeH9QpOpomwKLERQkACi8sJvXGHVZPbQ",
      ];

      /* Set required setting with curl configuration */
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
      curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

      /* Pass the additional values */
      curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

      /* Execute and close the curl connection */
      $result = curl_exec($ch);
      curl_close($ch);

      return $result;

    }
    catch (RequestException $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
  }

}
