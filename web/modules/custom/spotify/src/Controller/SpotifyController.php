<?php

namespace Drupal\spotify\Controller;

use Drupal\spotify\Service\ConnectionService;
use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Spotify routes.
 */
class SpotifyController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $url = 'https://api.spotify.com/v1/browse/new-releases?limit=10';

    $response = ConnectionService::request($url, 'GET');

    if ($response) {
      $result = json_decode($response);

      $data['title'] = 'NUEVOS LANZAMIENTOS';
      $data['items'] = $result->albums->items;

      $build['content'] = [
        '#theme' => 'featured_list',
        '#title' => 'Featured list',
        '#attached' => [
          'library' => [
            'spotify/spotify_theming.css',
          ],
        ],
        '#data' => $data,
      ];

      return $build;
    }
  }

}
