<?php

namespace Drupal\spotify\Controller;

use Drupal\spotify\Service\ConnectionService;
use Drupal\Core\Controller\ControllerBase;

/* use Drupal\Core\Url; */

/**
 * Returns responses for artist page.
 */
class ArtistPageController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build($id) {

    $conection = new ConnectionService();

    $url = 'https://api.spotify.com/v1/artists/' . $id;

    $urlAlbums = 'https://api.spotify.com/v1/artists/' . $id . '/albums?limit=10';

    $response = $conection->request($url, 'GET');

    if ($response) {
      $result = json_decode($response);

      $responseAlbums = $conection->request($urlAlbums, 'GET');

      if ($responseAlbums) {

        $rs = json_decode($responseAlbums);
        $data['artist'] = $result;
        $data['items'] = $rs->items;

        $build['content'] = [
          '#theme' => 'artist_page',
          '#title' => 'Artista',
          '#attached' => [
            'library' => [
              'spotify/spotify_artist.css',
            ],
          ],
          '#data' => $data,
        ];
      }
    }

    return $build;

  }

}
